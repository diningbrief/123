package edu.nccu.misds.stu102306070.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

//Modified from HW11
public class Website 
{
	private static int MAX_DEPTH = 2;
	private static int MAX_CHILDREN = 2;
	
	public String url;
	public String name;
	public float localScore;
	public float globalScore;
	
	
	private Website parent;
	private ArrayList<Website> children;
	
	private String content;
	
	public Website(String url)
	{
		this(url,"Unknown");
	}
	public Website(String url,String name)
	{
		this.url = url;
		this.name = name;
		this.children = new ArrayList<Website>();
	}
	
//	public Website(String url,String name,float score)
//	{
//		this.url = url;
//		this.name = name;
//		this.score = score;
//	}
	@Override
	public String toString()
	{
		return "["+url+","+name+","+localScore+","+globalScore+"]";
	}
	
	public Website getParent()
	{
		return parent;
	}
	public ArrayList<Website> getChildren()
	{
		return children;
	}
	public int getDepth()
	{
		if(getParent()==null)
		{
			return 0;
		}
		return getParent().getDepth()+1;
	}
	
	//HW3 & HW10
	private String fetchContent() throws IOException
	{
		String retVal = "";
		URL u = new URL(url);
		URLConnection conn = u.openConnection();
		conn.setRequestProperty("User-agent", "Chrome/7.0.517.44");
		InputStream in = conn.getInputStream();
		InputStreamReader inReader = new InputStreamReader(in,"utf-8");
		BufferedReader bufReader = new BufferedReader(inReader);
		
		String line = null;
		while((line=bufReader.readLine())!=null)
		{
			retVal += line;
		}
		
		return retVal;
		/*
		while(true)
		{
			line = bufReader.readLine();
			if(line == null)
			{
				break;
			}
			
			//do something...
		}
		*/
	}
	
	public void evaluate(List<Keyword> keywords)
	{
		System.out.println("Evaluating: "+url);
		try 
		{
			//HW3: fetch content by using http-request
			if(content==null)
			{
				content = fetchContent();
			}
			
			//parse html
			Document doc = Jsoup.parse(content);
			
			//extract <title> as name
			Elements titles = doc.select("title");
			if(titles.size()>0)
			{
				
				name = titles.get(0).text();
				System.out.println(name);
			}
			
			for(Keyword k : keywords)
			{
				//HW3: count the times of a keyword appears in one page
				int times = count(k.name);
				localScore += times*k.weight;
				k.count +=times;
			}
			
			if(getDepth()<Website.MAX_DEPTH)
			{
				extendNextLayer(doc,keywords);
			}
		} 
		catch (IOException e) 
		{
			//this web cannot be evaluated
			name = "PageNotFound";
			localScore = 0;
		}
	}
	
	//HW3
	public int count(String keyword) throws IOException
	{
		
		if(content == null)
		{
			content = fetchContent();
		}
		
		content = content.toUpperCase();
		keyword = keyword.toUpperCase();
		
		int retVal = 0;
		int from = 0;
		int found = -1;
		//while((found=content.indexOf(keyword,from))!= -1)
		while((found=indexOf(content,keyword,from))!= -1)
		{
			retVal++;
			from = found + keyword.length();
		}
		
		return retVal;
	}
	
	//HW3
	public int indexOf(String text,String pattern,int fromIndex)
	{
		
		if(text.length()==0 || pattern.length()==0)
		{
			return -1;
		}
		
		//build last occurrence mapping
		HashMap<Character, Integer> lastOccurMap = new HashMap<Character, Integer>();
		for(int i=pattern.length()-1;i>=0;i--)
		{
			char c = pattern.charAt(i);
			if(!lastOccurMap.containsKey(c))
			{
				//i is the last occurrence index of character c
				lastOccurMap.put(c, i);
			}
		}
		
		//find index of pattern by Boyer Moore Algorithm
		int i = pattern.length()-1 + fromIndex;
		int j= pattern.length()-1;
		
		while(i<=text.length()-1)
		{
			char t = text.charAt(i);
			char p = pattern.charAt(j);
			if(t==p)
			{
				if(j==0)
				{
					//match at i
					return i;
				}
				else
				{
					i--;
					j--;
				}
			}
			else
			{
				//character jump
				Integer lastOccur = lastOccurMap.get(t);
				if(lastOccur==null)
				{
					lastOccur=-1;
				}
				i = i + pattern.length() - Math.min(j, lastOccur+1);
				j = pattern.length() - 1;
			}
		}
		return -1;//no matching
	}
	
	public void extendNextLayer(Document doc,List<Keyword> keywords)
	{
		
		Elements links = doc.select("a[href]");//select <a> tags with 'href' attribute
		
		for(Element link : links)
		{
			if(getChildren().size()==Website.MAX_CHILDREN)
			{
				break;
			}
			
			//correct the extracted url
			
			String href = link.attr("href").toLowerCase().trim();
			
			if(href.startsWith("javascript:"))
			{
				//this is not a url but a javascript 
				continue;
			}
			if(!href.startsWith("http"))
			{
				//skip hyper link using relative path
				continue;
			}
			
			//append child
			Website child = new Website(href);
			this.children.add(child);
			child.parent = this;
		}
		
		
		for(Website childWeb : getChildren())
		{
			childWeb.evaluate(keywords);
		}
	}
	
	public void prettyPrint()
	{
		String result = "";
		for(int i=0;i<getDepth();i++)
		{
			result+="\t";
		}
		result+=this.toString();
		System.out.println(result);
		for(Website child : getChildren())
		{
			child.prettyPrint();
		}
	}
}
