package edu.nccu.misds.stu102306070.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GoogleQuery {

	private List<String> searchUrls;
	private List<String> searchKeywords;

	public GoogleQuery(List<String> searchKeywords) throws IOException {
		this.searchKeywords = searchKeywords;
		String searchKeyword = "";
		for (String tmp : searchKeywords) {
			searchKeyword = searchKeyword + tmp + " ";
		}
		searchKeyword = URLEncoder.encode(searchKeyword.trim(), "UTF-8");
		String a = "https://www.google.com/search?as_q="
				+ searchKeyword
				+ "&as_epq=&as_oq=&as_eq=&as_nlo=&as_nhi=&lr=&cr=&as_qdr=all&as_sitesearch=keeat.pixnet.net&as_occt=any&safe=images&as_filetype=&as_rights=&gws_rd=ssl";
		String b = "https://www.google.com/search?as_q="
				+ searchKeyword
				+ "&as_epq=&as_oq=&as_eq=&as_nlo=&as_nhi=&lr=&cr=&as_qdr=all&as_sitesearch=wkitty.pixnet.net&as_occt=any&safe=images&as_filetype=&as_rights=&gws_rd=ssl";
		String c = "https://www.google.com/search?as_q="
				+ searchKeyword
				+ "&as_epq=&as_oq=&as_eq=&as_nlo=&as_nhi=&lr=&cr=&as_qdr=all&as_sitesearch=sheena3943.pixnet.net&as_occt=any&safe=images&as_filetype=&as_rights=&gws_rd=ssl";
		searchUrls = new ArrayList<String>();
		searchUrls.add(a);
		searchUrls.add(b);
		searchUrls.add(c);

	}

	public String fetchContent1(String input) throws IOException {
		String retVal = "";
		URL u = new URL(input);
		URLConnection conn = u.openConnection();
		conn.setRequestProperty("User-agent", "Chrome/7.0.517.44");
		InputStream in = conn.getInputStream();
		InputStreamReader inReader = new InputStreamReader(in, "utf-8");
		BufferedReader bufReader = new BufferedReader(inReader);

		String line = null;
		while ((line = bufReader.readLine()) != null) {
			retVal += line;
		}
		return retVal;
	}

	public List<Subweb> searchResult() throws IOException {
		List<Subweb> result = new ArrayList<Subweb>();
			for (String key : searchUrls) {
			System.out.println(key);
			String searchResult = fetchContent1(key);
			int j =0;
			for (Element link : Jsoup.parse(searchResult).select(
					"li.g a:not([class])")) {
				if(j>2){
					break;
				}
				j++;
				String url = link.attr("href").intern().replace("/url?q=", "");
				System.out.println("processing " + url);
				String subContent = fetchContent1(url);
				String title = Jsoup.parse(subContent).title();
				int score = 0;
				
				for (int i = 0; i < searchKeywords.size(); i++) {
					int count = subContent.split(searchKeywords.get(i)).length - 1;
					int weight = searchKeywords.size() - i;
					score = score + count * weight;
				}
				
				Subweb sw = new Subweb(URLEncoder.encode(title,"UTF-8"), url, score);
				result.add(sw);
			}
		}
		Collections.sort(result, new Comparator<Subweb>() {
			@Override
			public int compare(Subweb o1, Subweb o2) {
				return o2.getScore() - o1.getScore();
			}
		});
		return result;
	}

}
