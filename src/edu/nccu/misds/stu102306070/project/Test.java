package edu.nccu.misds.stu102306070.project;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Test {
	public static void main(String[] args) throws IOException {
		String keywordString ="�Q�s";

		// String[] ks = keywordString.split(" ");
		List<String> keywords = Arrays.asList(keywordString.split(" "));
		GoogleQuery gq = new GoogleQuery(keywords);
		List<Subweb> result = gq.searchResult();
		for (Subweb sb : result) {
			System.out.println(sb.getTitle() + "</br>");
			System.out.println(sb.getUrl() + "</br>");
			System.out.println(sb.getScore() + "</br>");
			
		}
	}
}
