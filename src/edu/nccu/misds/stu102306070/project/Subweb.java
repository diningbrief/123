package edu.nccu.misds.stu102306070.project;

public class Subweb {
	private String title;
	private String url;
	private Integer score;

	public Subweb(String title, String url, Integer score) {
		super();
		this.title = title;
		this.url = url;
		this.score = score;
	}

	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}


	@Override
	public String toString() {
		return "Subweb [title=" + title + ", url=" + url + ", score=" + score
				+ "]";
	}

}
